#!/usr/bin/python
from __future__ import unicode_literals
from mp3_tagger import MP3File, VERSION_1, VERSION_2, VERSION_BOTH
import youtube_dl
import sys
import getopt
import os
import json
import datetime
import time

# -- Script start --
# Output colors
class bcolors:
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

# Helptext declaration
HT_D = """
-h                                   Print this menue
-v                                   Enable extended output"""

HT_BO = """
-d/--destination  '<destination>'    Enter the directory you want to save the mp3 files in (MUST BE ABSOLUTE, getcwd() by default)
-u/--url          '<url>'            Enter the valid video/playlist url [Required]
-o                                   With this option you can specify if existing files should be overwriten (False by default)
-e                                   Replaces all special characters in the filename
-t/--template     '<template>'       Insert your desired template for the file output (Default is '%(title)s.%(ext)s')
-S/--smart-artist '<seperators>'     Will auto select the artist with your given separetoren (<artist> <separator> <songname>)
-E/--create-dir   '<custom>'         Save the files to a custom directory under your destination
-I                                   Ignore errors on download (fe. 'ERROR: This video has been removed by the user')"""

HT_BC = """
-B/--body        '<path/body.yml>'   With this options you can enter a custom body for youtube-dl (experimental)"""

HT_C = """
-C/--check-file  '<path/filename>'   Check attributes (print). Insert the file to check including it's absolute path and extension
-D/--check-dir   '<path/directory>'  Check attributes (print). Insert the directory to check including it's absolute path and extension"""

HT_T = """
-a/--artist       '<artist>'         Insert the artist you want to tag your mp3 file with
-A/--album        '<album>'          Insert the album you want to tag your mp3 file with
-s/--song         '<name>'           Insert the name you want to tag your mp3 file with /auto/ will insert it's orgin name for each
-T/--track        '<X>/<Y>'          Insert the track number (example. 1/9) you want to tag your mp3 file with
-c/--comment      '<comment>'        Insert the comment you want to tag your mp3 file with
-y/--year         '<year>'           Insert the year  you want to tag your mp3 file with  /auto/ Value is the current year
-g/--genre        '<genre>'          Insert the genre you want to tag your mp3 file with

The options marked with /auto/ can be parametered with the value 'auto'. See behind /auto/ for each argument to see what happens
Example: python download.py -y 'auto' would result in being year the current date (2018)"""

HT_M = """
- MP3 Tagging: https://pypi.org/project/mp3-tagger/
- Youtube-DL: https://github.com/rg3/youtube-dl/blob/master/README.md
- Gitlab: https://gitlab.mgmt.local/scripts/python/youtube-to-mp3"""

# Argument Values
URL = None
IGNORE = False
DESTINATION = str(os.getcwd())
SPACES = False
OVERWRITE = False
DEBUG = False
TEMPLATE = '%(title)s.%(ext)s'
FILENAME = None
CUSTOM_BODY = None
CUSTOM_BODY_DATA = None
CUSTOM_BODY_EXISTS = None

# Tagging Values
TAGGING_ARRAY = []
INDEX = 0
TAGGING = False
ARTIST = None
ALBUM = None
SONG = None
TRACK = None
COMMENT = None
YEAR = None
GENRE = None
SMARTARTIST = None
ARTISTORDER = False

# Class declaration for logging
class Logger(object):
    def debug(self, msg):
        pass
    def warning(self, msg):
        pass
    def error(self, msg):
        print(msg)

# FUNCTION
def main(argv):
    global URL, DESTINATION, SPACES, OVERWRITE, DEBUG, TEMPLATE, ARTIST, ALBUM, SONG, TRACK, COMMENT, YEAR, GENRE, TAGGING, SMARTARTIST, ARTISTORDER, CUSTOM_BODY, IGNORE
    # Read Arguments
    try:
        opts, args = getopt.getopt(argv, "hvoyRIeu:d:t:a:A:C:s:T:c:y:g:D:S:B:", ["url=", "destination=", "template=", "artist=", "album=", "song=", "track=", "comment=", "year=", "genre=", "check-file=", "check-dir=", "body="])
    except getopt.GetoptError as E:
        print bcolors.FAIL + str(E) + bcolors.ENDC
        help(2)
    for opt, arg in opts:
        if opt == '-h':
            help(0)
        elif opt in ("-d", "--destination"):
            DESTINATION = arg
        elif opt in ("-u", "--url"):
            URL = arg
        elif opt in ("-e"):
            SPACES = True
        elif opt in ("-o"):
            OVERWRITE = True
        elif opt in ("-v"):
            DEBUG = True
        elif opt in ("-t", "--template"):
            TEMPLATE = str(arg)
        elif opt in ("-a", "--artist"):
            ARTIST = str(arg)
            TAGGING = True
        elif opt in ("-A", "--album"):
            ALBUM = str(arg)
            TAGGING = True
        elif opt in ("-s", "--song"):
            SONG = str(arg)
            TAGGING = True
        elif opt in ("-T", "--track"):
            TRACK = str(arg)
            TAGGING = True
        elif opt in ("-c", "--comment"):
            COMMENT = str(arg)
            TAGGING = True
        elif opt in ("-y", "--year"):
            YEAR = str(arg)
            TAGGING = True
        elif opt in ("-g", "--genre"):
            GENRE = str(arg)
            TAGGING = True
        elif opt in ("-S", "--smart-artist"):
            SMARTARTIST = str(arg)
            TAGGING = True
        elif opt in ("-R"):
            ARTISTORDER = True
            TAGGING = True
        elif opt in ("-O", "--options"):
            OPTIONS= str(arg)
        elif opt in ("-B", "--body"):
            CUSTOM_BODY = str(arg)
        elif opt in ("-I"):
            IGNORE = True
        elif opt in ("-C", "--check-file"):
            # Print attributes from given file
            mp3 = MP3File(str(arg))
            print(str(mp3.get_tags()))
            sys.exit(0)
        elif opt in ("-D", "--check-dir"):
            if os.path.isdir(str(arg)):
                for file in os.listdir(str(arg)):
                    if file.endswith(".mp3"):
                        # Test the file
                        print(bcolors.WARNING + "\n" + file + bcolors.ENDC)
                        mp3 = MP3File(str(os.path.join(str(arg), file)))
                        print(str(mp3.get_tags()))
                    else:
                        if DEBUG is True:
                            print(bcolors.WARNING + "Didn't find any files with the ending mp3 in " + arg + bcolors.ENDC)
                sys.exit(0)
            else:
                print(bcolors.FAIL + "The directory " + arg + " doesn't exist!" + bcolors.ENDC)
                sys.exit(2)

    # Check if url was given as argument
    if URL is None:
        print(bcolors.FAIL + "URL isn't set!" + bcolors.ENDC)
        help(2)

    # DESTINATION
    if not DESTINATION.endswith("/"):
        DESTINATION = str(DESTINATION + "/")

    # When destination start with ++, add this directory to getcwd()
    SEP = "++"
    if DESTINATION.startswith(SEP):
        DESTINATION = str(os.getcwd() + DESTINATION.split(SEP, 1)[1])

    # Check if directory is absolute + has slash at the end
    if not os.path.isabs(DESTINATION):
        print(bcolors.FAIL + "\nThe path " + DESTINATION + " isn't absolute" + bcolors.ENDC)
        sys.exit(2)

    # Output variables
    if DEBUG is True:
        # Output Vars
        print(bcolors.WARNING + "\nAvailable variables:" + bcolors.ENDC + "\nURL = " + str(URL) + "\nDESTINATION = " + str(DESTINATION) + "\nSPACES = " + str(SPACES) + "\nOVERWRITE = " + str(OVERWRITE) + "\nDEBUG = " +str(DEBUG) + "\nTEMPLATE = " + str(TEMPLATE) + "\nARTIST = " + str(ARTIST) + "\nALBUM = " + str(ALBUM) + "\nSONG = " + str(SONG) + "\nTRACK = " + str(TRACK) + "\nCOMMENT = " + str(COMMENT) + "\nYEAR = " + str(YEAR) + "\nGENRE = " + str(GENRE) + "\nSMART_ARTIST = " + str(SMARTARTIST) + "\nARTISTORDER = " + str(ARTISTORDER))

# Hook definition & Create API Object
def local_hook(d):
    # Global VAR declaration
    global DEBUG, TAGGING, TAGGING_ARRAY, INDEX, CUSTOM_BODY, DESTINATION

    # If ojcet is done downloading
    if d['status'] == 'finished':

        # When finished - feedback and save filename
        try:
            # Get Filename from response
            (name, ext) = os.path.splitext(d['filename'].encode('unicode_escape').decode('ascii'))

	    # DEBUG
            if DEBUG is True:
                # Print Feedback
                print(bcolors.WARNING + "\n" + name + " - " + bcolors.OKGREEN + "Done downloading, now converting ..." + bcolors.ENDC)

            # Get Filename from response
            FILENAME = str(name + ".mp3")

            # If Debug - Print json response
            if DEBUG is True:
                print("Local filename: " + DESTINATION + FILENAME)
                print("Your response: " + json.dumps(d))

            # If TAGGING TRUE
            if TAGGING is True:
                # Add File with it's filename to array
                TAGGING_ARRAY.append([])
                TAGGING_ARRAY[INDEX].append(str(DESTINATION + FILENAME))
                TAGGING_ARRAY[INDEX].append(name)
                INDEX = INDEX + 1

        # Exception
        except Exception as E:
            print bcolors.FAIL + str(E) + bcolors.ENDC
            sys.exit(2)

# Initalize download
def predownload():
    # Global VAR declaration
    global URL, DESTINATION, TEMPLATE, INTERVAL, SPACES, DEBUG, CUSTOM_BODY, CUSTOM_BODY_EXISTS, CUSTOM_BODY_DATA, DIRECTORY, IGNORE

    # Stage Output
    if DEBUG is True:
        print(bcolors.WARNING + "\n-------------- Stage: Pre Download --------------" + bcolors.ENDC)

    # Change to destination (Check if directory exists)
    try:
        # Change to directory
        os.chdir(DESTINATION)
    except Exception as E:
        print bcolors.FAIL + str(E) + bcolors.ENDC
        sys.exit(2)

    # Declaration of the var to build your body
    YDL_OPTS=''
 
    # Differ between custom and default Body
    if CUSTOM_BODY is None:
    
        # That's the base of the OPTIONS
        YDL_OPTS = { 
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'nooverwrites': OVERWRITE,
        'restrictfilenames': SPACES,
        'outtmpl': TEMPLATE,
        'verbose': DEBUG, 
        'ignoreerrors': IGNORE,
        }

    else:
        # Check if the given file exists (ONLY ONCE)
        if CUSTOM_BODY_EXISTS is None:
            # Check if the file exists to set the global var
            if os.path.isfile(CUSTOM_BODY):
                try:
                    # Open the custom body file and redirect it's content to your data v
                    with open(CUSTOM_BODY, 'r') as cb:
                        CUSTOM_BODY_DATA=cb.read().replace('\n', '')

                    # Try to load the json (syntax-check)
                    json.loads(CUSTOM_BODY_DATA)

                    # File was found
                    if DEBUG is True:
                        print(bcolors.WARNING + "The custom body " + CUSTOM_BODY + "was successfully imported!:\n" + str(json.dumps(json.loads(CUSTOM_BODY_DATA))) + bcolors.ENDC)

                    # Box values to global var
                    CUSTOM_BODY_EXISTS = True

                except Exception as E:
                    print(bcolors.FAIL + str(E) + bcolors.ENDC)
                    sys.exit(2)

            # The given file wasn't found
            else:
                print(bcolors.FAIL + "The file " + CUSTOM_BODY + " doesn't exist!" + bcolors.ENDC)
                sys.exit(2)

        # Now the script assumes, that the data body was set already
        # Check again if custom body data was really set
        if CUSTOM_BODY_DATA is not None:
            try:
                # Dump json to check if it's valid
                json.dumps(json.loads(CUSTOM_BODY_DATA))

                #  DEBUG
                if DEBUG:
                    print(bcolors.WARNING + "Syntax of custom data body OK!" + bcolors.ENDC)

            except Exception as E:
                print(bcolors.FAIL + str(E) + bcolors.ENDC)
                sys.exit(2)

        # Something went wrong there!
        else:
            print(bcolors.FAIL + "The Data body is None. This means your data couldn't have been imported!\n" + "CUSTOM_BODY_DATA=" + str(CUSTOM_BODY_DATA) + bcolors.ENDC)
            sys.exit(2)

        # Import that shit to youtube-var with required attr
        #YDL_OPTS = json.dumps(json.loads(CUSTOM_BODY_DATA))


    # Define it as the body
    YDL_OPTS['forcefilename'] = True
    YDL_OPTS['forceurl'] = True
    YDL_OPTS['progress_hooks'] = [local_hook]
    YDL_OPTS['logger'] = Logger()

    # Debug the final body
    if DEBUG is True:
        print("\n Your data body:\n" + bcolors.WARNING + str(YDL_OPTS) + bcolors.ENDC)

    # Call the function to download
    youtubedl(YDL_OPTS)

# Youtube-DL
def youtubedl(options):
    # Global VAR declaration
    global URL

    # Stage Output
    if DEBUG is True:
        print(bcolors.WARNING + "\n---------------- Stage: Download ----------------\n" + bcolors.ENDC)

    # Declare your youtube-dl options
    with youtube_dl.YoutubeDL(options) as ydl:
        ydl.download([URL])

# TAGGING
def tag():

    # Global variables
    global ARTIST, ALBUM, SONG, TRACK, COMMENT, YEAR, GENRE, TAGGING, TAGGING_ARRAY, SMARTARTIST, ARTISTORDER

    # Stage
    if DEBUG is True:
        print(bcolors.WARNING + "\n----------------- Stage: Tagging ----------------" + bcolors.ENDC)

    # If any tagging attribute was set, this condition is True
    if TAGGING:
        # Insert Array
        for row in TAGGING_ARRAY:
            filepath = str(row[0])
            songname = str(row[1].replace('_', ' '))
            
            # DEBUG: Show each rows values
            if DEBUG is True:
                print("\n" + bcolors.WARNING + songname + bcolors.ENDC)

            # Check if file exists
            if os.path.isfile(filepath) is True:
                # Try to set attributes
                try:
                    # Match variable
                    matched=False

                    # Let's tag
                    mp3 = MP3File(filepath)
                    
                    # Smart Artist will split the artist name + name of the song with your seperators
                    if SMARTARTIST is not None:
                        # For seperators
                        ARTIST_SEPERATORS = SMARTARTIST.split(" ", 100)  # 100 is the maximum elments that can  be splitted

                        # For Separtor
                        for sep in ARTIST_SEPERATORS:
                            # Try with seperator
                            try:
                                # Box the splited values
                                if ARTISTORDER is True:
                                    name = str(songname.rsplit(sep, 1)[0])
                                    artist = str(songname.rsplit(sep, 1)[1])
                                else:
                                    name = str(songname.rsplit(sep, 1)[1])
                                    artist = str(songname.rsplit(sep, 1)[0])

                                # Cut first Spaces
                                if name.startswith(" "):
                                    name = name[1:]
                                    
                                # Cut first spaces
                                if artist.startswith(" "):
                                    artist = artist[1:]
                                
                                # Cut lst Spaces
                                if name.endswith(" "):
                                    name = name[:-1]

                                # Cut last spaces
                                if artist.endswith(" "):
                                    artist = artist[:-1]

                                # Insert values
                                mp3.artist = str(artist)
                                mp3.song = str(name)

                                # Smart Artist response
                                if DEBUG is True:
                                    print(bcolors.OKGREEN + "\nFound seperation with " + sep + " for " + songname + bcolors.ENDC)
                                    print(bcolors.WARNING + "Values:\n" + bcolors.ENDC + "Name = " + name + "\nArtist = " + artist)

                                # MATCHED
                                matched = True
                            except:
                                # Smart Artist response
                                if DEBUG is True:
                                    print(bcolors.ERROR + "Cloudn't find any seperation with " + sep + " for " + songname + bcolors.ENDC)
                                continue

                    # If none Matched try
                    if matched is False:

                        # Set the artist
                        if ARTIST is not None:
                            # Tag the file's artist field
                            mp3.artist = str(ARTIST)

                        # Set the song name
                        if SONG is not None:
                            if SONG.lower() != "auto":
                                # Tag the file's song field
                                mp3.song = str(SONG)
                            else:
                                # Tag the file's song field
                                mp3.song = str(songname)

                    # Set the album
                    if ALBUM is not None:
                        # Tag the file's album field
                        mp3.album = str(AlBUM)

                    # Set the track
                    if TRACK is not None:
                        # Tag the file's track field
                        mp3.track = str(TRACK)

                    # Set the year
                    if YEAR is not None:
                        # Empty var
                        currentyear=''
                        if YEAR.lower() != "auto":
                            currentyear = str(YEAR)
                        else:
                            now = datetime.datetime.now()
                            currentyear = str(now.year)

                        # Tag the file's year field
                        mp3.year = currentyear

                    # Set the comment
                    if COMMENT is not None:
                        # Tag the file's track field
                        mp3.track = str(COMMENT)

                    # Set the genre
                    if GENRE is not None:
                        # Tag the file's track field
                        mp3.track = str(GENRE)

                    # By default selected tags in both versions.
                    mp3.set_version(VERSION_BOTH)

                    # After the tags are edited, you must call the save method.
                    mp3.save()

                    # DEBUG: Print all tags
                    if DEBUG is True:
                        print("Tags:" + str(mp3.get_tags()))


                # Anything went wrong
                except Exception as E:
                    print(bcolors.FAIL + str(E) + bcolors.ENDC)
                    sys.exit(2)

            # When file doen't exist
            else:
                print(bcolors.FAIL + "The file " + filepath + " doesn't exist!\n" + bcolors.ENDC)
                # Make next array entry
                continue

    # When TAGGING is false
    else:
        print(bcolors.WARNING + "No tagging available (no attributes set!)" + bcolors.ENDC)

# Show Help + Exit
def help(code):
    global HT_D, HT_BO, HT_BC, HT_C, HT_T, HT_M

    # Print default otpions
    print (bcolors.WARNING + "\nDefault options:\n" + bcolors.ENDC + HT_D)

    # Print body options
    print (bcolors.WARNING + "\nBody (youtube-dl) options:\n" + bcolors.ENDC + HT_BO)

    # Print custom body options
    print (bcolors.WARNING + "\nCustom Body (youtube-dl):\n" + bcolors.ENDC + HT_BC)

    # Print checks
    print (bcolors.WARNING + "\nAvailable checks:\n" + bcolors.ENDC + HT_C)

    # Print Tagging options
    print (bcolors.WARNING + "\nTagging options:\n" + bcolors.ENDC + HT_T)

    # Print more information
    print (bcolors.WARNING + "\nMore information:\n" + bcolors.ENDC + HT_M + "\n")

    # Exit with given code
    sys.exit(code)

# Call main
if __name__ == "__main__":
    main(sys.argv[1:])
    predownload()
    tag()
    print(bcolors.WARNING + "\n--------------------- Done ---------------------" + bcolors.ENDC)
