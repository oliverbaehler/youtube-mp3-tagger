# youtube-mp3-tagger

## Why
I am having an self-hosted music library. I had always to manually download all the music files and tag them as well. As you might know/guess this is very time comesuming. And also, the next day the channel will upload an video you would love to have in your library. But that must wait till the next time you will finde the time to download and tag this channel again. And that kept going over ages in my case. So now I decided to do something against it. So I came up with this script in this repository. Luckily I didn't have to write the whole functionalities like downloading etc. There are already existing  python modules I made usee of. With this im mean the very popular module [youtube-dl](https://github.com/rg3/youtube-dl) for downloading and mp3 conversion. For tagging the mp3 files I made use of the python library [mp3-tagger](https://github.com/artcom-net/mp3-tagger). Now I am storing my music library on my local Synology NAS. I am able to make tasks, which trigger this script daily with my needed arguments. The only thing I have to do, is to create the task with the right arguments. Everything else will now be done by the script.

## Who could use this
If you are looking for a simple script that you could schedule daily and would keep you always updated from downloading the mp3 files from your favourite youtube music channel, then your search might end right here.

## Arguments
You can call the help menue:

    python youtube-mp3-tagger.py -h

There is already a short describtion for each available argument.

Nevertheless the following table contains all arguments with a little more describtion and information:

| Argument | Required | Value | Default value | Describiton | Example |
|:-:|:-:|:-:|:-:|---|:-:|
| **-h** | no | no | no | Print the help menue with all arguments listed. | x |
| **-v** | no | no | no | Print extende output (Debug mode). Recommended! | x |
| **-d**/**--destination** | no | yes | os.getcwd() | This argument is used as the destination, where the mp3 files should be saved to. Must always be an absolute path. Unless you wanna go deeper in the tree from the current directory you are, you could use something as shown in the example. | `-d "++/library"` |
| **-u**/**--url** | yes* | yes | no | This argument is used as the url, which youtube-dl uses to download. Wheter it's a single video or a playlist, doesn't matter. | -u "https://www.youtube.com/watch?v=bYJJ14c3NKc" |
| **-o** | no | no | False | This argument is used as indication, wheter existing files should be overwritten or not. Overwritting is disabled by default (False). | x |
| **-e** | no | no | True | This argument is used by youtube-dl, which let's youtube-dl remove chars like &, spaces etc. | x|
| **-t**/**--template** | no | yes | %(title)s.%(ext)s | This argument can take an output template used by youtube-dl. The default value will save the mp3 file as 'filename'.mp3. But you can create your own custom output template ([See here](https://github.com/rg3/youtube-dl#output-template)). | -t "%(title)s-%(id)s.%(ext)s" |
| **-S**/**--smart-artist** | no | yes | False | This argument isn't as smart as it might claim. Infact it's very simple. If you have video titles such as for exp. "Night - Liar" then this parameter will define Night as **artist"" and Liar as **songname** in the files attributes. The only thing you have to insert, is the separator. You can insert multiple separators separated by spaces. If no seperator hits, the value from **-a** and **-s** will be used. That's why it's a good idea to insert the **-a** and **-s** option as backup, for those who don't hit any separtor | -S "- _ :" |
| **-O** | no | no | False | This argument extends the -S argument. It just reverses the mapping from (Default: <artist> <separator> <songname>) to (Default: <songname> <separator> <artist>). The -S argument must be set, for this argument to work. | x |
| **-B**/**--body** | no | yes | no | Enter your custom youtube-dl body (path to yaml file as value). See which options for your youtube-dl bodies are available [here](https://github.com/rg3/youtube-dl/blob/3e4cedf9e8cd3157df2457df7274d0c842421945/youtube_dl/YoutubeDL.py#L137-L312). Note that the following options will be set anyway because they are required for the script to work (**forcefilename**=True, **forceurl**=True, **progress_hooks**=[local_hook], **logger**=Logger()) | -b "./mybody.yaml" |
| **-a**/**--artist** | no | yes | no  | This argument is used to tag your mp3 file. It will tag the artist field with the value you deliver.  |  -a "anyartist"|
| **-A**/**--album** | no | yes | no | This argument is used to tag your mp3 file. It will tag the album field with the value you deliver. | -A "anyalbum" |
| **-s**/**--song**  | no | yes | auto | This argument is used to tag your mp3 file. It will tag the song field with the value you deliver. With delivering the value "auto" each file is tagged with it's escaped download name (Default). | -s "auto" |
| **-T**/**--track**  | no | yes | no | This argument is used to tag your mp3 file. It will tag the track field with the value you deliver. | -T "1/9" |
| **-c**/**--comment** | no | yes | no | This argument is used to tag your mp3 file. It will tag the comment field with the value you deliver. | -c "This is a random comment" |
| **-y**/**--year** | no | yes | auto | This argument is used to tag your mp3 file. It will tag the song field with the value you deliver. With delivering the value "auto" the current year is elected as value. | -y "auto" / -y "2016" |
| **-g**/**--genre** | no | yes | no | This argument is used to tag your mp3 file. It will tag the genre field with the value you deliver. | -g "Drum & Bass"  |
| **-C**/**--check-file**   | no | yes | no | With this argument you can check one specific mp3 file. With check as a simple output of all on this existing tags meant. The path to the file must be absolute. | -C "/check/this/file.mp3" |
| **-D**/**--check-dir** | no | yes | no | With this argument you can check one directory with mp3 files. With check as a simple output of all on this existing tags meant. Every mp3 extensioned file in your given directory will be printed. The path to the file must be absolute. | -D "/check/this/dir" |

## Commands
When executing the script you should always run in the background by simply appending a & to the end of the command (Linux distros):

    python youtube-mp3-tagger.py .... &

## Examples
Here some simple examples with explenation:

## First example

    python youtube-mp3-tagger.py -u "https://www.youtube.com/watch?v=HXp_ZYNQpbA&index=46&list=PLTdft-zEsMRwh76v1KlWdauLg3zVg2eRL" -d "++/my-playlist" -o --artist "superior" -g "chillout" -S "-"


### Downloads

### MP3 checks
Print all attributes for the specified file:

    python youtube-mp3-tagger.py -C "/path/absolute/audio.mp3"

Print all attributes for all files with the extension '*.mp3' in the given absolute path

    python youtube-mp3-tagger.py -D "/path/absolute/"

## Requirments
You need python to be installed:

[https://www.python.org/downloads/](https://www.python.org/downloads/)

### youtubde-dl

For downloading the videos and converting them into mp3 format you need youtube_dl library (install with pip):

    sudo -H pip install (--upgrade) youtube-dl

See the origin:
[https://github.com/rg3/youtube-dl/blob/master/README.md#readme](https://github.com/rg3/youtube-dl/blob/master/README.md#readme)

### mp3-tagger

For tagging your downloaded mp3 files you need mp3-tagger (install with pip):

    sudo -H pip install (--upgrade) mp3-tagger

Pypi:
[https://pypi.org/project/mp3-tagger/](https://pypi.org/project/mp3-tagger/)

## Known Errors

### ffprobe or avprobe
if you are getting this error:

    youtube_dl.utils.DownloadError: ERROR: ffprobe or avprobe not found. Please install one.

You should install one of this packages (apt install ffprobe/avprobe).
See th following threat:

[https://stackoverflow.com/questions/30770155/ffprobe-or-avprobe-not-found-please-install-one](https://stackoverflow.com/questions/30770155/ffprobe-or-avprobe-not-found-please-install-one)


## Contribute
Feel free to create issues. I'd really appricate feedback, because I am just a newbie in python scripting. Of course if you miss a feature, I will do my best to help you out with it. Any idea/feedback is very welcome :).

## Inspiration/Based on
This script wouldn't be possible without these two modules:

* **[https://github.com/rg3/youtube-dl](youtubde-dl)**
* **[https://github.com/artcom-net/mp3-tagger](mp3-tagger)**

Big thanks!

## Usage
Downloading videos or related formats might by illegal in the state/country you are located in. By using this script the author can't by charged for any law breaks done by the consumer. It's the consumer's own responseability using this script. With the usage of this script you (as a consumer)  automatically agree with these conditions.
